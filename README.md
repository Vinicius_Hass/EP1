//___________________________________________________________________
//---------Trabalho de Programação 1----------01/2016----------------
//---------Materia de Orientação a Objetos // Codigo: 195341---------
//---------Professor: Ranato Coral Sampario--------------------------
//---------Aluno: Vinicius Guimarães Hass // Matricula 10/0021751----
//___________________________________________________________________

This program make the extraction of a hidden text in the imagens of type pgm (P5)
Or it make the extraction of a hidden imagen at a layer of one color in the imagens of type ppm (P6)

/-------------------------------------------------------------------

Methods of usage this Program

For use of function call use (./bin/main)

Any file the program need to create, will be created in actual/doc/

For use of program follow the mensagens in the screen

If the image with secret in a subfolder write the folder/.../image.format
Ex.: doc/lena.pgm or doc/secret.ppm

If the image with secret is in any folder write all path, maximum 255 characteres to send the path
/-------------------------------------------------------------------
Easy use for gray imagem :
$ make
$ make run
$ 1
$ doc/lena.pgm (example)

DONE!

/-------------------------------------------------------------------
Easy use for gray imagem :
$ make
$ make run
$ 2
$ doc/segredo.ppm (example)
$ 1	(example for red color)

DONE!

/-------------------------------------------------------------------
Simple to use read the commands on the screen
/-------------------------------------------------------------------
If the program can not extract the correct answer to the gray image just close it and try again, program have some issue yet not can be found

