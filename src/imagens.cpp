//___________________________________________________________________
//---------Trabalho de Programação 1----------01/2016----------------
//---------Materia de Orientação a Objetos // Codigo: 195341---------
//---------Professor: Ranato Coral Sampario--------------------------
//---------Aluno: Vinicius Guimarães Hass // Matricula 10/0021751----
//___________________________________________________________________
#include "imagens.hpp"

using namespace std;

char * imagens::getMagicNumber(){
	return MagicNumber;
}

void imagens::setMagicNumber(char * MagicNumber){
	this->MagicNumber = MagicNumber;
}

int * imagens::getDimensoes(){
	return Dimensoes;
}

void imagens::setDimensoes(int Dimensoes[2]){
	this->Dimensoes=Dimensoes;
}

int imagens::getProfundidade(){
	return Profundidade;
}

void imagens::setProfundidade(int Profundidade){
	this->Profundidade=Profundidade;
}

string imagens::getComentario(){
	return Comentario;
}

void imagens::setComentario(string Comentario){
	this->Comentario=Comentario;	
}

char *** imagens::getCamada(){
	return Camada;
}

imagens::imagens(){
};

imagens::~imagens(){
	delete[](MagicNumber);
	delete[](Dimensoes);
	delete[](Camada);
};