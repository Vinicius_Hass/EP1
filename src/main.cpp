//___________________________________________________________________
//---------Trabalho de Programação 1----------01/2016----------------
//---------Materia de Orientação a Objetos // Codigo: 195341---------
//---------Professor: Ranato Coral Sampario--------------------------
//---------Aluno: Vinicius Guimarães Hass // Matricula 10/0021751----
//___________________________________________________________________
#include <stdlib.h>		//For use of exit()
#include <iostream>		//For use of cin // cout // delete() // new
#include <fstream>		//For use of open() // is_open() // close () //
#include "grayimage.hpp"//For use of getMagicNumber() // getComentario // getDimensoes ...
#include "rgbimage.hpp"	//For use of getMagicNumber() // getComentario // getDimensoes ...
#include "decifraGray.hpp"
#include "decifraRGB.hpp"
//declaração por causa de cin cout e outras chamadas
using namespace std;

int main(int argc, char const *argv[])
{
	int Seletor = 4;		//Variavel para navegar pelo Menu

	do{
		system("clear");	//Limpeza da Tela
		cout << "-------------------------------------------------------" << endl;
		cout << "----------Escolha uma opçao do menu abaixo:------------" << endl;
		cout << "1 - Localizar texto em uma imagem em escala de Cinza---" << endl;
		cout << "2 - Ver Apenas uma camada de cor em uma imagem colorida" << endl;
		cout << "0 - Sair-----------------------------------------------" << endl;
		cout << "Opçao:  ";
		cin >> Seletor;
		cout << endl;

		switch (Seletor){
		case 0:	
			cout << "Fechando o Programa" << endl;
		break;
		case 1:{
			//Vavel para armazenar Arquivo em Buffer
			grayimage * Imagem;
			// Pegando nome do arquivo
			char NomeArquivo[256];	//Alocação estatica do nome do arquio
			cout << "Entre com o endereço e nome do arquivo do arquivo: (pasta/imagem)"<<endl;
			cin.clear();	//Preparando cin para entrada do usuario
      		cin.ignore(10000,'\n');
			std::cin.getline (NomeArquivo,256);	//Pegando nome do arquivo
			// Abrindo o Arquivo
			std::fstream ObjetoArquivo;
			ObjetoArquivo.open (NomeArquivo, fstream::in);
			//Testando se o Arquivo foi aberto
			if (ObjetoArquivo.is_open()) {
		    }
			else {
				//Caso o Arquivo não seja aberto corretamente o Usuario é avisado e o programa reinicia;
				cout << "Error ao abrir o arquivo"<< endl;
				cout << "Pressione qualquer tecla para voltar ao menu inicial"<< endl;
				cin.clear();	//Preparando cin para entrada do usuario
      			cin.ignore(10000,'\n');
				cin.ignore(1);
				break;
			}
			//Caso o arquivo seja aberto corretamente, é verificado se o formato é o certo.
			{
				//Verifica se o arquivo aberto esta no formato correto esperado
				char MagicNumber[3];
				
				ObjetoArquivo.getline(MagicNumber,3);
				ObjetoArquivo.close();

				if ((MagicNumber[0] == 'P') && (MagicNumber[1] == '5'))
				{
					Imagem = new grayimage(NomeArquivo);
				}
				else{
					//Caso o formato seja errado é avisado para o usuario e o programa reinicia
					cout << "Tipo errado de arquivo" << endl;
					cout << "Pressione qualquer tecla para voltar ao menu inicial"<< endl;
					cin.clear();	//Preparando cin para entrada do usuario
      				cin.ignore(10000,'\n');
					cin.ignore(1);
					break;
				}
			}

			//Remove o texto escondido da Imagem
			decifraGray Texto(Imagem->getCamada(), Imagem->getDimensoes(),Imagem->getComentario());
			ObjetoArquivo.open ("doc/MensagemEscondida.txt", fstream::out | fstream::trunc);

			if (ObjetoArquivo.is_open()) { //Verificando se o arquivo aonde o texto vai ser salvo foi aberto corretamente
				ObjetoArquivo << Texto.getMensagemGray();
				ObjetoArquivo.close();
				cout << "A mensagem foi salva com sucesso em ./doc/MensagemEscondida.txt" << endl;
				cout << "Pressione qualquer tecla para voltar ao menu inicial" << endl;
				cin.clear();	//Preparando cin para entrada do usuario
      			cin.ignore(10000,'\n');
				cin.ignore(1);
		    }
			else {
				//Caso o Arquivo não seja aberto corretamente o Usuario é avisado e o programa reinicia;
				cout << "Error ao abrir ao criar o arquivo para salva a Mensagem" << endl;
				cout << "A Mensagem escondida é: " << endl;
				cout << Texto.getMensagemGray() << endl << endl;
				cout << "Pressione qualquer tecla para voltar ao menu inicial" << endl;
				cin.clear();	//Preparando cin para entrada do usuario
      			cin.ignore(10000,'\n');
				cin.ignore(1);
				delete(Imagem);
				break;
			}

		}break;
		case 2:{
			//Vavel para armazenar Arquivo em Buffer
			rgbimage * Imagem;
			// Pegando nome do arquivo
			char NomeArquivo[256];
			cout << "Entre com o endereço e nome do arquivo do arquivo: (pasta/imagem)" << endl;
			cin.clear();	//Preparando cin para entrada do usuario
      		cin.ignore(10000,'\n');
			std::cin.getline (NomeArquivo,256);	//Pegando nome do arquivo
			// Abrindo o Arquivo
			std::fstream ObjetoArquivo;
			ObjetoArquivo.open (NomeArquivo, fstream::in);
			//Testando se o Arquivo foi aberto
			if (ObjetoArquivo.is_open()) {
		    }
			else {
				//Caso o Arquivo não seja aberto corretamente o Usuario é avisado e o programa reinicia;
				cout << "Error ao abrir o arquivo"<< endl;
				cout << "Pressione qualquer tecla para voltar ao menu inicial"<< endl;
				cin.clear();	//Preparando cin para entrada do usuario
      			cin.ignore(10000,'\n');
				cin.ignore(1);
				break;
			}
			//Caso o arquivo seja aberto corretamente, é verificado se o formato é o certo.
			{
				//Verifica se o arquivo aberto esta no formato correto esperado
				char MagicNumber[3];

				ObjetoArquivo.getline(MagicNumber,3);
				ObjetoArquivo.close();

				if ((MagicNumber[0] == 'P') && (MagicNumber[1] == '6'))
				{
					Imagem = new rgbimage(NomeArquivo);
				}
				else{
					//Caso o formato seja errado é avisado para o usuario e o programa reinicia
					cout << "Tipo errado de arquivo" << endl;
					cout << "Pressione qualquer tecla para voltar ao menu inicial"<< endl;
					cin.clear();	//Preparando cin para entrada do usuario
      				cin.ignore(10000,'\n');
					cin.ignore(1);
					delete(Imagem);
					break;
				}
			}
			//Limpando a Tela
			system("clear");
			//Alocando Variaveis Locais
			int Cor;	//Variavel para selecionar a unica cor a permanecer na imagem
			//Menu de selecão de Cor
			cout << "-------------------------------------------------------" << endl;
			cout << "------------Escola a camada a ser Mantida--------------" << endl;
			cout << "------------1 -> Vermelho------------------------------" << endl;
			cout << "------------2 -> Verde---------------------------------" << endl;
			cout << "------------3 -> Azul----------------------------------" << endl;
			cout << "Entre com a Opção: ";
			cin >> Cor;
			
			if (Cor == 1)
			{
				cout << endl << "Foi Selecionada a Cor Vermelha" << endl;
			}
			else if (Cor == 2)
			{
				cout << endl << "Foi Selecionada a Cor Verde" << endl;
			}else if (Cor == 3)
			{
				cout << endl << "Foi Selecionada a Cor Azul" << endl;
			}else{
				cout << endl << "Opção Invalida" << endl;
				cout << "Pressione qualquer tecla para voltar ao menu inicial"<< endl;
				cin.clear();	//Preparando cin para entrada do usuario
      			cin.ignore(10000,'\n');
				cin.ignore(1);
				delete(Imagem);
				break;
			}

			//Remove a Cor das camadas não deseijadas
			decifraRGB Texto(Imagem->getCamada(), Imagem->getDimensoes(), Cor - 1);
			ObjetoArquivo.open ("doc/MensagemEscondida.ppm", fstream::out | fstream::trunc);
			//Verifica se o arquivo aonde vai ser salvo as imagem com o segredo revelado foi aberta corretamente
			if (ObjetoArquivo.is_open()) {

				ObjetoArquivo << Imagem->getMagicNumber() << endl;
				ObjetoArquivo << *(Imagem->getDimensoes()) << " " << *(Imagem->getDimensoes()+1) << endl;
				ObjetoArquivo << Imagem->getProfundidade() << endl;

				char *** Intensidade = Texto.getCamadaRGB();
					for (int i = 0; i < *(Imagem->getDimensoes()); ++i)
				{
					for (int j = 0; j < *(Imagem->getDimensoes()+1) ; ++j)
					{
						for (int k = 0; k < 3; ++k)
						{
							ObjetoArquivo << Intensidade[i][j][k] << flush;;
						}
					}
				}

				ObjetoArquivo.close();
				cout << "A mensagem foi salva com sucesso em ./doc/MensagemEscondida.ppm" << endl;
				cout << "Pressione qualquer tecla para voltar ao menu inicial" << endl;
				cin.clear();	//Preparando cin para entrada do usuario
      			cin.ignore(10000,'\n');
				cin.ignore(1);
				delete[](Intensidade);
		    }
			else {
				//Caso o Arquivo não seja aberto corretamente o Usuario é avisado e o programa reinicia;
				cout << "Error ao abrir ao criar o arquivo para salva a Imagem" << endl;
				cout << "Pressione qualquer tecla para voltar ao menu inicial" << endl;
				cin.clear();	//Preparando cin para entrada do usuario
      			cin.ignore(10000,'\n');
				cin.ignore(1);
			}
			delete(Imagem);

		}break;
		default:
			Seletor = 1;
			cout << "Opção Invalida" << endl;
			cout << "Pressione qualquer tecla para voltar ao menu inicial" << endl;
			cin.clear();
			cin.ignore(10000,'\n');
			cin.ignore(1);
		break;
		}

	}while(Seletor != 0);

	return 0;
}