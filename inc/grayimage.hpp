//___________________________________________________________________
//---------Trabalho de Programação 1----------01/2016----------------
//---------Materia de Orientação a Objetos // Codigo: 195341---------
//---------Professor: Ranato Coral Sampario--------------------------
//---------Aluno: Vinicius Guimarães Hass // Matricula 10/0021751----
//___________________________________________________________________
#ifndef GRAYIMAGE_HPP
#define GRAYIMAGE_HPP
#include "imagens.hpp"
#include <iostream>
#include <fstream>
//Classe filha de "imagens" para alocação na memoria dos atributos
//de imagens preto e branco
class grayimage: public imagens
{
	// Atributos 
public:
	// Metodos
	grayimage(const char * Arquivo);	//Construtor do objeto
	~grayimage();	//destrutor virtual que chama destutor da classe imangens
};

#endif