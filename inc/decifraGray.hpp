//___________________________________________________________________
//---------Trabalho de Programação 1----------01/2016----------------
//---------Materia de Orientação a Objetos // Codigo: 195341---------
//---------Professor: Ranato Coral Sampario--------------------------
//---------Aluno: Vinicius Guimarães Hass // Matricula 10/0021751----
//___________________________________________________________________
#ifndef	DECIFRAGRAY_HPP
#define	DECIFRAGRAY_HPP
//Bibliotecas
#include <iostream>
#include <string>
#include <stdio.h>		//atoi();
#include <locale>		//isdigit
#include "decifra.hpp"
//declaração por causa das strings
using namespace std;
// Classe filha de "decifra" usada para gerar a solução para 
// mensagens escondidas em imagens preto e branco
class decifraGray : public decifra
{
public:
	// Metodos
	decifraGray(char *** Camada, int * Dimensoes,string Comentario);
};

#endif

