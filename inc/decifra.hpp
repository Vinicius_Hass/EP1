//___________________________________________________________________
//---------Trabalho de Programação 1----------01/2016----------------
//---------Materia de Orientação a Objetos // Codigo: 195341---------
//---------Professor: Ranato Coral Sampario--------------------------
//---------Aluno: Vinicius Guimarães Hass // Matricula 10/0021751----
//___________________________________________________________________
#ifndef DECIFRA_HPP
#define DECIFRA_HPP
//Bibliotecas
#include <iostream>
#include <string>
//declaração por causa das strings
using namespace std;
// Classe abstrata criada para conter Atributos das classes filhas
// e funções generiacas para retornar o valor dos atributos
class decifra
{
protected:
	// Atributos
	string MensagemGray;
	char *** CamadaRGB;
	// Metodos
public:
	char *** getCamadaRGB();	//Retorna o endereço do inicio do ponteiro da solução para imagens RGB
	string getMensagemGray();	//Retorna a string contendo a mensagem escondida na imagem preto e branca
	decifra();					//função virtual de criada da classe
};

#endif

