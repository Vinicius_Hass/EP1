//___________________________________________________________________
//---------Trabalho de Programação 1----------01/2016----------------
//---------Materia de Orientação a Objetos // Codigo: 195341---------
//---------Professor: Ranato Coral Sampario--------------------------
//---------Aluno: Vinicius Guimarães Hass // Matricula 10/0021751----
//___________________________________________________________________
#ifndef	DECIFRARGB_HPP
#define	DECIFRARGB_HPP
//Bibliotecas
#include <iostream>
#include "decifra.hpp"
// Classe filha de "decifra" usada para gerar a solução para 
// mensagens escondidas em imagens coloridas
class decifraRGB : public decifra
{
public:
	// Metodos
	decifraRGB(char *** Camada, int * Dimensoes, int Cor);
};

#endif

